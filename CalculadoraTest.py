from unittest import TestCase

from Calculadora import Calculadora

class CalculadoraTest(TestCase):
    def test_sumar(self):
        self.assertEqual(Calculadora().sumar(""),0,"Cadena Vacia")

    def test_sumar_unNumero(self):
        self.assertEqual(Calculadora().sumar("1"),1,"Un Numero")

    def test_sumar_cadenaConUnNumero(self):
        self.assertEqual(Calculadora().sumar("1"),1,"Un Numero")
        self.assertEqual(Calculadora().sumar("2"),2,"Un Numero")

    def test_sumar_cadenaConDosNumeros(self):
        self.assertEqual(Calculadora().sumar("1,2"),3,"Dos Numero")

    def test_sumar_cadenaConNNumeros(self):
        self.assertEqual(Calculadora().sumar("1,2,3,6"),12,"N Numeros")

    def test_sumar_cadenaConNNumerosConSeparadores(self):
        self.assertEqual(Calculadora().sumar("5,2&4:1:2&8"),22,"N Numeros distintos separadores")